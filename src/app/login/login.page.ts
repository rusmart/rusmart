import { Component, OnInit } from '@angular/core';

import '@codetrix-studio/capacitor-google-auth';
//import { Plugins } from '@capacitor/core';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { AuthserviceService } from '../service/authservice.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from'rxjs/operators';
import { Storage } from '@capacitor/storage';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  userInfo = null;

  constructor(private authservice: AuthserviceService) {
      GoogleAuth.initialize();
  }

  ngOnInit() {
  //  this.getData();
  
  this.authservice.getAccessToken().then(
     data=>{console.log(data)
    });
  }
  async getData(){
    const {value} = await Storage.get({ key: 'accesstoken' });
    const user = value
    console.log(user);
  }

  async googleSignup() {
    //const googleUser = await Plugins.GoogleAuth.signIn(null) as any;
    let stdcode ;
    const googleUser = await GoogleAuth.signIn() as any;
    console.log('my user: ', googleUser);
    this.userInfo = googleUser;
    stdcode = (this.userInfo.email.substring(0,10));
    console.log("login page");
    this.authservice.googleAuthHeader(this.userInfo.authentication.idToken,stdcode).subscribe(
      res => {
        console.log(res);
    
      }
    )

    //console.log(stdcode.substring(0,10));
   // console.log(this.userInfo.authentication.idToken);
  }

}
