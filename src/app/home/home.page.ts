import { Component } from '@angular/core';

import '@codetrix-studio/capacitor-google-auth';
//import { Plugins } from '@capacitor/core';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  userInfo = null;

  constructor() {
      GoogleAuth.initialize();
  }

  async googleSignup() {
    //const googleUser = await Plugins.GoogleAuth.signIn(null) as any;
    const googleUser = await GoogleAuth.signIn() as any;
    console.log('my user: ', googleUser);
    this.userInfo = googleUser;
  }
}
