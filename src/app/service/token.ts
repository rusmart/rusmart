

    export interface Message {
        audience: string;
        email: string;
        expires_in: number;
        issued_to: string;
        user_id: string;
        verified_email: boolean;
    }

    export interface AuthResponse {
        message: Message;
    }

