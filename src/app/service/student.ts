
// export interface Student {
//     studentCode: string;
//     firstNameThai: string;
//     lastNameThai: string;
//     firstNameEng: string;
//     lastNameEng: string;
//     facultyCode: string;
//     facultyNameThai: string;
//     facultyNameEng: string;
//     majorCode: string;
//     majorNameThai: string;
//     majorNameEng: string;
// }


export interface StudentProfile {
    studentInfomation: StudentInfomation[];
  }
  
  export interface StudentInfomation {
    studentCode: string;
    firstNameThai: string;
    lastNameThai: string;
    firstNameEng: string;
    lastNameEng: string;
    facultyCode: string;
    facultyNameThai: string;
    facultyNameEng: string;
    majorCode: string;
    majorNameThai: string;
    majorNameEng: string;
  }
  
  
  
  