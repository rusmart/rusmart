import { Injectable } from '@angular/core';
import { StudentProfile } from './student';
// import { environment } from 'src/environments/environment';
// import { HttpClient } from '@angular/common/http';
// import { Observable, of } from 'rxjs';
// import { catchError, tap,map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  studentProfile: StudentProfile;

  constructor(private http: HttpClient) { }

  fetchProfileStudent(): Observable<StudentProfile>{
    return this.http.get<StudentProfile>(environment.api+'/profile/6299999991').pipe(
      map(data => {
        console.log(data.studentInfomation[0].facultyNameThai);
        data.studentInfomation[0].facultyNameThai = 'นิติ';
        this.setFac(data.studentInfomation[0].facultyNameThai);
        return data;
      }
    //  ,
    //   map(
    //     res => {
    //       console.log(res.studentInfomation[0].facultyNameThai);
    //       res.studentInfomation[0].facultyNameThai = 'นิติ';
    //       return res;
    //     }
    //   )
    )
    );
  }

 public setFac(fac: string) {
    localStorage.setItem('fac', fac);
  }

  public getFac(): string {
    return localStorage.getItem('fac');
  }
}
