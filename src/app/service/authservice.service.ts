import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthResponse } from './token';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthserviceService {


  constructor(private http: HttpClient) { }
  
  googleAuth(idtoken: string): Observable<any>{
  //console.log("google serviec!!")
  let body = {};
    return this.http.post<AuthResponse>(environment.googleAuth+`?id_token=${idtoken}`,body).pipe(
      map(data => {
       // console.log(data);
        return data;
      }
        ))}

        private async setAccessToken(accessToken:string){
            await Storage.set({
            key: 'accessToken',
            value: accessToken,
          });

        }
        private async setRefreshToken(refreshToken:string){
            await  Storage.set({
            key: 'refreshToken',
            value: refreshToken,
          });
        }

         async getAccessToken() {
          const {value} = await Storage.get({ key: 'accessToken' });
          return value
        }
         async getRefreshToken() {
          const {value} = await Storage.get({ key: 'refreshToken' });
          return value
        }

    googleAuthHeader(idtoken: string,stdcode: string){
      const headers ={ 
      'Content-Type': 'application/json',
      'Authorization' : 'Bearer '+idtoken

   };


   const body = {'std_code':stdcode }

  return this.http.post<AuthResponse>(environment.googleAuth,body,{headers}).pipe(
    map(data => {
      let email:string = data.message.email;
      let user_id = data.message.user_id;
      this.setAccessToken(email);
      this.setRefreshToken(user_id);
      return data;
    }
      ))
    }


}
