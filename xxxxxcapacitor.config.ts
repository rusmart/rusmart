import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.dawut.login',
  appName: 'loginwut',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
